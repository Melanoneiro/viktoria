// This node transforms the range readings from the ultrasound sensors
// into coordinates in the robot's base frame of reference

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string>
#include <sstream>
#include <tf/transform_listener.h>
#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include "sensor_msgs/Joy.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/PointStamped.h"

geometry_msgs::PointStamped range_point;
geometry_msgs::PointStamped base_point;
volatile bool transform = 0;
volatile bool publish = 0;
volatile int range_sensor = 0;

// This function makes use of the ROS tf-package
// Range readings are transformed into the robot's
// base frame of reference here
void transformRange(const tf::TransformListener& listener){
  try{
    listener.transformPoint("base_link", range_point, base_point);

    // Check which sensor was transformed
    if(range_point.header.frame_id == "range_sensor_1_link")
      range_sensor = 1;
    if(range_point.header.frame_id == "range_sensor_2_link")
      range_sensor = 2;
    if(range_point.header.frame_id == "range_sensor_3_link")
      range_sensor = 3;
    if(range_point.header.frame_id == "range_sensor_4_link")
      range_sensor = 4;
    if(range_point.header.frame_id == "range_sensor_5_link")
      range_sensor = 5;
    if(range_point.header.frame_id == "range_sensor_6_link")
      range_sensor = 6;
    if(range_point.header.frame_id == "range_sensor_7_link")
      range_sensor = 7;
    if(range_point.header.frame_id == "range_sensor_8_link")
      range_sensor = 8;

    // Point is transformed, it can now be published
    transform = 0;
    publish = 1;
  }catch(tf::TransformException& ex){
    ROS_ERROR("Received an exception trying to transform a point to \"base_link\": %s", ex.what());
  }
  return;
}

// Range readings from the sensors are received here
void rangeCallback(const sensor_msgs::Range::ConstPtr& range_msg){
  range_point.header.frame_id = range_msg->header.frame_id;
  range_point.point.x = 0;
  range_point.point.y = 0;
  range_point.point.z = range_msg->range;

  // Range reading needs to be transformed, can not yet be published
  transform = 1;
  publish = 0;
  return;
}

int main(int argc, char **argv){
  // Init ROS
  ros::init(argc, argv, "viktoria_tf_ranges");
  ros::NodeHandle n;
  ros::Publisher tf_range_1_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_1", 1000);
  ros::Publisher tf_range_2_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_2", 1000);
  ros::Publisher tf_range_3_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_3", 1000);
  ros::Publisher tf_range_4_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_4", 1000);
  ros::Publisher tf_range_5_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_5", 1000);
  ros::Publisher tf_range_6_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_6", 1000);
  ros::Publisher tf_range_7_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_7", 1000);
  ros::Publisher tf_range_8_pub = n.advertise<geometry_msgs::PointStamped>("/viktoria/tf_range_8", 1000);
  ros::Subscriber ranges_sub = n.subscribe("/viktoria/ranges", 1000, rangeCallback);
  tf::TransformListener listener(ros::Duration(10));

  while(ros::ok()){
    geometry_msgs::Twist vel_msg;
    // Transform range to point if ready
    if(transform)
      transformRange(listener);
    // Publish transformed point if ready
    if(publish){
      switch(range_sensor){
        case 1:
          tf_range_1_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 2:
          tf_range_2_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 3:
          tf_range_3_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 4:
          tf_range_4_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 5:
          tf_range_5_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 6:
          tf_range_6_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 7:
          tf_range_7_pub.publish(base_point);
          range_sensor = 0;
          break;
        case 8:
          tf_range_8_pub.publish(base_point);
          range_sensor = 0;
          break;
        default:
          break;
      }
    }
    ros::spinOnce();
  }
  return 0;
}
