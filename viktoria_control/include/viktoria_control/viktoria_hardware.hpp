// Code largely based on the diffbot example from the diff_drive_controller test files

#ifndef VIKTORIA_HARDWARE_H
#define VIKTORIA_HARDWARE_H

// ROS
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/JointState.h>
#include <std_srvs/Empty.h>

// ROS control
#include <controller_manager/controller_manager.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <realtime_tools/realtime_buffer.h>

// NaN
#include <limits>

// string handling
#include <sstream>
#include <string>
#include <vector>

// Math functions
#include <math.h>

class viktoria : public hardware_interface::RobotHW{
public:
  // Constructor
  viktoria(std::vector<std::string> wheel_names);

  // Init functions
  void initJoints(std::vector<std::string> wheel_names);

  // ROS control function
  bool startCallback(std_srvs::Empty::Request& /*req*/, std_srvs::Empty::Response& /*res*/);
  bool stopCallback(std_srvs::Empty::Request& /*req*/, std_srvs::Empty::Response& /*res*/);
  void read();
  void write();

  // Callbacks
  void bumperCallback(const sensor_msgs::Joy::ConstPtr& bumper_msg);
  void jointStateCallback(const sensor_msgs::JointState::ConstPtr& joint_state_msg);

  // Functions setting/getting state variables
  ros::Time getTime() const;
  ros::Duration getPeriod() const;
  void isRunning(bool state);
  bool isRunning() const;
  void isInitialized(bool state);
  bool isInitialized() const;
  void isInEStop(bool);
  bool isInEStop() const;
  void hitObject(bool);
  bool hitObject() const;

private:
  // Hardware definitions
  static const uint8_t num_wheels_ = 2;
  const double controller_fq_ = 100.0;

  // ROS handles
  ros::NodeHandle nh_;
  ros::Subscriber sub_bumper_;
  ros::Subscriber joint_state_sub_;
  ros::Publisher joint_command_pub_;

  // Variables for ROS control
  ros::ServiceServer start_srv_; 
  ros::ServiceServer stop_srv_;

  // Hardware Interfacing variables
  hardware_interface::JointStateInterface jnt_state_interface_;
  hardware_interface::VelocityJointInterface jnt_vel_interface_;

  sensor_msgs::JointState joint_state_;
  sensor_msgs::JointState joint_command_;

  // Robot state variables
  double cmd_[num_wheels_]; // [rad/s]
  double pos_[num_wheels_]; // [rad]
  double vel_[num_wheels_]; // [rad/s]
  double eff_[num_wheels_]; // the effort applied; not yet implemented

  bool initialized_;
  bool running_;
  bool e_stop_;
  bool hit_object_;
};

#endif