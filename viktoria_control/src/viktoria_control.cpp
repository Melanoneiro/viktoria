// Control node running the hardware class

#include <viktoria_control/viktoria_hardware.hpp>

int main(int argc, char **argv){
  //Init ROS and variables
  ros::init(argc, argv, "viktoria_control_node");
  ros::NodeHandle nh;
  
  std::vector<std::string> wheel_names;
  wheel_names.push_back("wheel_left_joint");
  wheel_names.push_back("wheel_right_joint");
  //wheel_names.push_back("wheel_left_joint_motor");
  //wheel_names.push_back("wheel_right_joint_motor");
  viktoria robot(wheel_names);
  if(!robot.isInitialized())
  {
    ROS_ERROR("Failed to initialize robot hardware!");
    return -1;
  }

  controller_manager::ControllerManager cm(&robot, nh);

  ros::Rate rate(1 / robot.getPeriod().toSec());
  ros::AsyncSpinner spinner(1);
  spinner.start();
  while(ros::ok())
  {
    // Run the control loop of the robot
    // Syntax is: read - update controller manager - write - sleep
    robot.read();
    cm.update(robot.getTime(), robot.getPeriod());
    robot.write();
    rate.sleep();
  }
  spinner.stop();

  return 0;
}
