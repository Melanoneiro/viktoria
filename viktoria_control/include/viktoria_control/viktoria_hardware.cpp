#include "viktoria_hardware.hpp"

/* -------------------------------------------------- */
/* Constructor                                        */
/* -------------------------------------------------- */

viktoria::viktoria(std::vector<std::string> wheel_names):
  // Initialization list
  initialized_(true),
  running_(true),
  e_stop_(false),
  hit_object_(false),
  start_srv_(nh_.advertiseService("/viktoria/start", &viktoria::startCallback, this)),
  stop_srv_(nh_.advertiseService("/viktoria/stop", &viktoria::stopCallback, this)),
  joint_command_pub_(nh_.advertise<sensor_msgs::JointState>("/viktoria/joint_commands", 100, this)),
  joint_state_sub_(nh_.subscribe("/viktoria/joint_states", 100, &viktoria::jointStateCallback, this)),
  sub_bumper_(nh_.subscribe("/viktoria/bumper", 100, &viktoria::bumperCallback, this))
{
  // Constructor
  initJoints(wheel_names);

  ROS_INFO("Robot initialized.");
}

/* -------------------------------------------------- */
/* Init functions                                     */
/* -------------------------------------------------- */

void viktoria::initJoints(std::vector<std::string> wheel_names)
{
  // init joint handling variables
  std::fill_n(cmd_, num_wheels_, 0.0);
  std::fill_n(pos_, num_wheels_, 0.0);
  std::fill_n(vel_, num_wheels_, 0.0);
  std::fill_n(eff_, num_wheels_, 0.0);

  // connect and register joint state & velocity interface
  for(unsigned int i = 0; i < num_wheels_; i++)
  {
    joint_command_.name.push_back(wheel_names[i]);
    joint_command_.position.push_back(0.0);
    joint_command_.velocity.push_back(0.0);
    joint_command_.effort.push_back(0.0);

    joint_state_.name.push_back(wheel_names[i]);
    joint_state_.position.push_back(0.0);
    joint_state_.velocity.push_back(0.0);
    joint_state_.effort.push_back(0.0);

    hardware_interface::JointStateHandle state_handle(wheel_names[i],
                                                      &pos_[i],
                                                      &vel_[i],
                                                      &eff_[i]);
    jnt_state_interface_.registerHandle(state_handle);

    hardware_interface::JointHandle vel_handle(jnt_state_interface_.getHandle(wheel_names[i]), &cmd_[i]);
    jnt_vel_interface_.registerHandle(vel_handle);
  }
  registerInterface(&jnt_state_interface_);
  registerInterface(&jnt_vel_interface_);
}

/* -------------------------------------------------- */
/* ROS control functions                              */
/* -------------------------------------------------- */

bool viktoria::startCallback(std_srvs::Empty::Request& /*req*/, std_srvs::Empty::Response& /*res*/)
{
  isRunning(true);
  ROS_INFO("Robot started");
  return true;
}

bool viktoria::stopCallback(std_srvs::Empty::Request& /*req*/, std_srvs::Empty::Response& /*res*/)
{
  isRunning(false);
  ROS_INFO("Robot stopped");
  return true;
}

void viktoria::read()
{
  for(unsigned int i = 0; i < num_wheels_; i++)
  {
    vel_[i] = joint_state_.velocity[i];
    pos_[i] += joint_state_.velocity[i] * getPeriod().toSec();
    eff_[i] = joint_state_.effort[i];
  }
}

void viktoria::write()
{
  if(isRunning())
  {
    for(unsigned int i = 0; i < num_wheels_; i++)
    {
      if(isInEStop())
      {
        joint_command_.velocity[i] = 0.0;
      }
      else
      {
        joint_command_.velocity[i] = cmd_[i];
      }
    }
    joint_command_.header.stamp = ros::Time::now();
    joint_command_pub_.publish(joint_command_);
  }
}

/* -------------------------------------------------- */
/* Callbacks                                          */
/* -------------------------------------------------- */

void viktoria::bumperCallback(const sensor_msgs::Joy::ConstPtr& bumper_msg)
{
  // Check E-Stop button
  if(1 == bumper_msg->buttons[0]){
    isInEStop(true);
  }
  else
  {
    isInEStop(false);
  }

  // Evaluate displacement of bumper
  if(sqrt(pow(bumper_msg->axes[0], 2) + pow(bumper_msg->axes[1], 2)) > 0.009)
    hitObject(true);
  else
    hitObject(false);
}

void viktoria::jointStateCallback(const sensor_msgs::JointState::ConstPtr& joint_state_msg)
{
  for(unsigned int i = 0; i < num_wheels_; i++)
  {
    if(0 != joint_state_.name[i].compare(joint_state_msg->name[i]))
      ROS_ERROR("Error reading Joint states: Order of joints in message is wrong!");
    joint_state_.position[i] = joint_state_msg->position[i];
    joint_state_.velocity[i] = joint_state_msg->velocity[i];
    joint_state_.effort[i] = joint_state_msg->effort[i];
  }
}


/* -------------------------------------------------- */
/* Functions setting/getting state variables          */
/* -------------------------------------------------- */

ros::Time viktoria::getTime() const
{
  return ros::Time::now();
}
  
ros::Duration viktoria::getPeriod() const
{
  return ros::Duration(1 / controller_fq_);
}

void viktoria::isRunning(bool state)
{
  running_ = state;
}
bool viktoria::isRunning() const
{
  return running_;
}

void viktoria::isInitialized(bool state)
{
  initialized_ = state;
}
bool viktoria::isInitialized() const
{
  return initialized_;
}

void viktoria::isInEStop(bool state)
{
  e_stop_ = state;
}
bool viktoria::isInEStop() const
{
  return e_stop_;
}

void viktoria::hitObject(bool state)
{
  hit_object_ = state;
}
bool viktoria::hitObject() const
{
  return hit_object_;
}